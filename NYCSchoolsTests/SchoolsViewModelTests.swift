//
//  SchoolsViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Dipendra Khatri on 2/13/22.
//

import XCTest
@testable import NYCSchools

class SchoolsViewModelTests: XCTestCase {
    var viewModel: SchoolsViewModel!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewModel = SchoolsViewModel(apiClient: APIClientMock())
        viewModel.loadSchoolsDetails(for: 0)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        viewModel = nil
    }
    
    func testCurrentPage() throws {
        XCTAssertEqual(viewModel.currentPage, 0)
    }

    func testNumberOfRows() throws {
        XCTAssertEqual(viewModel.numberOfRows, 5)
    }
    
    func testShouldFetchNextPage() throws {
        XCTAssertEqual(viewModel.shouldFetchNextPage(index: 1), false)
        XCTAssertEqual(viewModel.shouldFetchNextPage(index: 4), true)
    }
    
    func testRowViewModel() throws {
        XCTAssertNotNil(viewModel.rowViewModel(for: IndexPath(row: 0, section: 0)))
        XCTAssertNil(viewModel.rowViewModel(for: IndexPath(row: 5, section: 0)))
    }
    
    func testFilterSchools() throws {
        viewModel.filterSchools(for: "")
        XCTAssertEqual(viewModel.numberOfRows, 5)
        XCTAssertNotNil(viewModel.rowViewModel(for: IndexPath(row: 0, section: 0)))
        XCTAssertNil(viewModel.rowViewModel(for: IndexPath(row: 5, section: 0)))
        
        viewModel.filterSchools(for: "Clinton")
        XCTAssertEqual(viewModel.numberOfRows, 1)
        XCTAssertNotNil(viewModel.rowViewModel(for: IndexPath(row: 0, section: 0)))
        XCTAssertNil(viewModel.rowViewModel(for: IndexPath(row: 1, section: 0)))
    }
    
    func testHandleRowSelect() throws {
        viewModel.handleRowSelect(for: IndexPath(row: 0, section: 0))
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
