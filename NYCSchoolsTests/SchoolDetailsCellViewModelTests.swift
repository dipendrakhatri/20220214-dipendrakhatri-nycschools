//
//  SchoolDetailsCellViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Dipendra Khatri on 2/13/22.
//

import XCTest
@testable import NYCSchools

class SchoolDetailsCellViewModelTests: XCTestCase {

    var viewModel: SchoolsViewModel!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewModel = SchoolsViewModel(apiClient: APIClientMock())
        viewModel.loadSchoolsDetails(for: 0)
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        viewModel = nil
    }
    
    func testSchoolDetails() throws {
        let cellViewModel = viewModel.rowViewModel(for: IndexPath(row: 0, section: 0))
        XCTAssertEqual(cellViewModel?.schoolName, "Clinton School Writers & Artists, M.S. 260")
        XCTAssertEqual(cellViewModel?.address, "10 East 15th Street, Manhattan NY 10003")
        XCTAssertEqual(cellViewModel?.phoneNumber, "212-524-4360")
        XCTAssertEqual(cellViewModel?.program, "Program: M.S. 260 Clinton School Writers & Artists")
        XCTAssertEqual(cellViewModel?.interest, "Interest: Humanities & Interdisciplinary")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
