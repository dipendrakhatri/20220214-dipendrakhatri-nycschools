//
//  SchoolDetailsViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Dipendra Khatri on 2/13/22.
//

import XCTest
@testable import NYCSchools

class SchoolDetailsViewModelTests: XCTestCase {

    var viewModel: SchoolDetailsViewModel?
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let viewModel = SchoolsViewModel(apiClient: APIClientMock())
        viewModel.loadSchoolsDetails(for: 0)
        if let cellViewModel = viewModel.rowViewModel(for: IndexPath(row: 0, section: 0)) {
            self.viewModel = SchoolDetailsViewModel(cellViewModel.schoolDetails, nil)
        }
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        viewModel = nil
    }
    
    func testSchoolDetails() throws {
        XCTAssertNotNil(viewModel?.schoolInfo)
        XCTAssertNotNil(viewModel?.satMathScore)
        XCTAssertNotNil(viewModel?.satReadingScore)
        XCTAssertNotNil(viewModel?.satWritingScore)
        XCTAssertNotNil(viewModel?.addtionalInfoText)
        XCTAssertNotNil(viewModel?.location)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
