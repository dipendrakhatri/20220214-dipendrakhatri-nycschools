//
//  NSAttributedString+Extension.swift
//  NYCSchools
//
//  Created by Dipendra Khatri on 2/12/22.
//

import Foundation
import UIKit

extension NSAttributedString {
    func applying(attributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        let copy = NSMutableAttributedString(attributedString:self)
        let range = (string as NSString).range(of: string)
        copy.addAttributes (attributes, range: range)
        return copy
    }
    
    func font(with fontSize: CGFloat) -> NSAttributedString {
        applying(attributes: [.font: UIFont.systemFont(ofSize: fontSize)])
    }
    
    func bolded(with fontSize: CGFloat) -> NSAttributedString {
        applying(attributes: [.font: UIFont.boldSystemFont(ofSize: fontSize)])
    }
}
