//
//  APIClientMock.swift
//  NYCSchools
//
//  Created by Dipendra Khatri on 2/11/22.
//

import Foundation

/// Mock the network calls and respond
struct APIClientMock: APIClientConfigurable {
    /// Read local json file
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        return nil
    }
    
    /// Use this method to download the data from provided URL
    ///
    /// - Parameter urlString: API URL
    ///             params: Query parameters for API call
    ///             completion: completion handler for API response
    /// - Returns: Void
    func getData<T>(urlString: String, params: [String : String], completion: @escaping (Result<[T], Error>) -> Void) where T : Decodable {
        var data: Data?
        
        if urlString == nycSchoolsListURL {
            data = readLocalFile(forName: "SchoolDetailsResponse")
        } else {
            data = readLocalFile(forName: "SATScoreResponse")
        }
        
        if let data = data {
            do {
                let object = try JSONDecoder().decode([T].self, from: data)
                completion(.success(object))
            } catch let decoderError {
                completion(.failure(decoderError))
            }
        }
    }
}
