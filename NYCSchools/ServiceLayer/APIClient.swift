//
//  APIClient.swift
//  NYCSchools
//
//  Created by Dipendra Khatri on 2/11/22.
//

import Foundation

let nycSchoolsListURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
let nycSchoolSATInfoURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

/// Validation Error Enum
enum ValidationError: Error {
    case invalidURL
}

/// Defines the interface for APIClient
protocol APIClientConfigurable {
    func getData<T: Decodable>(urlString: String, params: [String: String], completion: @escaping (Result<[T], Error>) -> Void)
}

/// Handles the network calls and respond
struct APIClient: APIClientConfigurable {
    /// Use this method to download the data from provided URL
    ///
    /// - Parameter urlString: API URL
    ///             params: Query parameters for API call
    ///             completion: completion handler for API response
    /// - Returns: Void
    func getData<T: Decodable>(urlString: String, params: Parameters, completion: @escaping (Result<[T], Error>) -> Void){
        guard let url = URL(string: urlString) else { return completion(.failure(ValidationError.invalidURL))}
        let urlRequest = URLRequest(url: url).encode(with: params)
        let defaultSession = URLSession(configuration: .default)
        defaultSession.dataTask(with: urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                }
                if let data = data {
                    do {
                        let object = try JSONDecoder().decode([T].self, from: data)
                        completion(.success(object))
                    } catch let decoderError {
                        completion(.failure(decoderError))
                    }
                }
            }
        }.resume()
    }
}
