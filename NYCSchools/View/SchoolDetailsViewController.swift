//
//  SchoolDetailsViewController.swift
//  NYCSchools
//
//  Created by Dipendra Khatri on 2/12/22.
//

import Foundation
import UIKit
import MapKit

/// A view that displays school details
class SchoolDetailsViewController: UIViewController {
    /// Stores UI elements
    @IBOutlet var contentView: UIView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var schoolInfoTextView: UITextView!
    @IBOutlet var satMathScoreLabel: UILabel!
    @IBOutlet var satReadingScoreLabel: UILabel!
    @IBOutlet var satWritingScoreLabel: UILabel!
    @IBOutlet var addtionalInfoLabel: UILabel!
    
    /// Stores the view model object
    var viewModel: SchoolDetailsViewConfigurable! {
        didSet {
            setupUI()
        }
    }
    
    /// view lifecycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.alpha = 0
        contentView.layer.cornerRadius = 15
        contentView.transform = CGAffineTransform(translationX: 0.0, y: contentView.frame.height)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.checkAction))
        backgroundView.addGestureRecognizer(gesture)
        setupUI()
    }
    
    /// view lifecycle method
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3) {
            self.backgroundView.alpha = 0.8
            self.contentView.transform = .identity
        }
    }
    
    /// Setup the UI to show school details
    private func setupUI() {
        if !isViewLoaded { return }
        schoolInfoTextView.attributedText = viewModel.schoolInfo
        satMathScoreLabel.attributedText = viewModel.satMathScore
        satReadingScoreLabel.attributedText = viewModel.satReadingScore
        satWritingScoreLabel.attributedText = viewModel.satWritingScore
        addtionalInfoLabel.attributedText = viewModel.addtionalInfoText
        
        if let location = viewModel.location {
            mapView.setRegion(location, animated: true)
        }
    }
    
    /// Hide school details view on tap of background view
    @objc
    func checkAction(sender : UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            self.backgroundView.alpha = 0
            self.contentView.transform = CGAffineTransform(translationX: 0.0, y: self.contentView.frame.height)
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}
