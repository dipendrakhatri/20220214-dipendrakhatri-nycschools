//
//  SchoolDetailsCell.swift
//  NYCSchools
//
//  Created by Dipendra Khatri on 2/11/22.
//

import Foundation
import UIKit

/// A UITableViewCell that displays selective school details
class SchoolDetailsCell: UITableViewCell {
    var viewModel: SchoolDetailsCellViewConfigurable!
    @IBOutlet var schoolNameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var programLabel: UILabel!
    @IBOutlet var interestLabel: UILabel!
    
    /// Store view model and update the text in cell
    ///
    /// - Parameter viewModel: cell view model object
    /// - Returns: Void
    func setupViewModel(_ viewModel: SchoolDetailsCellViewConfigurable) {
        self.viewModel = viewModel
        schoolNameLabel.text = viewModel.schoolName
        addressLabel.text = viewModel.address
        phoneNumberLabel.text = nil
        programLabel.text = viewModel.program
        interestLabel.text = nil
    }
    
    /// Reset the cell for reuse
    ///
    override func prepareForReuse() {
        schoolNameLabel.text = nil
        addressLabel.text = nil
        phoneNumberLabel.text = nil
        programLabel.text = nil
        interestLabel.text = nil
        super.prepareForReuse()
    }
}
