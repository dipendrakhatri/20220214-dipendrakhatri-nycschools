//
//  SchoolsViewController.swift
//  NYCSchools
//
//  Created by Dipendra Khatri on 2/11/22.
//

import UIKit

/// A view that displays list for schools
class SchoolsViewController: UITableViewController {
    /// Stores the search controller
    let searchController = UISearchController(searchResultsController: nil)
    /// Stores the view model object
    var viewModel: SchoolsViewConfigurable!
    /// Stores the boolean that indicates initial load
    var initialLoad = true

    ///  view lifecycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        /// Setup Search Controller
        setupSearchController()
        
        viewModel = SchoolsViewModel()
        viewModel.showAlert = { [weak self] msg in
            self?.showAlert(with: msg)
        }
        viewModel.dataReady = { [weak self] in
            self?.tableView.tableFooterView = nil
            self?.tableView.reloadData()
        }
        viewModel.showSchoolDetailsScreen = { [weak self] viewModel in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let schoolDetailsVC = storyboard.instantiateViewController(withIdentifier: "SchoolDetailsViewController") as? SchoolDetailsViewController else { return }
            schoolDetailsVC.viewModel = viewModel
            schoolDetailsVC.modalPresentationStyle = .overFullScreen
            self?.present(schoolDetailsVC, animated: false, completion: nil)
        }
    }
    
    ///  view lifecycle method
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if initialLoad {
            initialLoad = false
            viewModel.loadSchoolsDetails(for: 0)
        }
    }
    
    ///  Setup the search controller and add it to navigation
    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search School by Name"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    /// create spinner to show loading in UITableView footer
    private func createSpinnerFooter() -> UIView {
        let footerView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.size.width, height: 50.0)))
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()
        return footerView
    }
    
    /// Show alert with provided message
    private func showAlert(with msg: String) {
        let alert = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true, completion: nil)
    }
}

/// extension that conform the UITableView DataSource and Delegate methods
extension SchoolsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.handleRowSelect(for: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolDetailsCell") as? SchoolDetailsCell,
           let rowViewModel = viewModel.rowViewModel(for: indexPath) {
            cell.setupViewModel(rowViewModel)
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.shouldFetchNextPage(index: indexPath.row) {
            tableView.tableFooterView = createSpinnerFooter()
        }
    }
}

/// UISearchResultsUpdating conformance
extension SchoolsViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    viewModel.filterSchools(for: searchBar.text!)
    tableView.reloadData()
  }
}
