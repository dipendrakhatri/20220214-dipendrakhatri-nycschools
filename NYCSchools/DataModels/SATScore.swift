//
//  SATScore.swift
//  NYCSchools
//
//  Created by Dipendra Khatri on 2/13/22.
//

import Foundation

/*
 {
     "dbn": "01M292",
     "school_name": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
     "num_of_sat_test_takers": "29",
     "sat_critical_reading_avg_score": "355",
     "sat_math_avg_score": "404",
     "sat_writing_avg_score": "363"
 }
 */

struct SATScore: Decodable {
    let dbn: String?
    let numOfTestTakers: String?
    let readingAvgScore: String?
    let mathAvgScore: String?
    let writingAvgScore: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case numOfTestTakers = "num_of_sat_test_takers"
        case readingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
      }
}
