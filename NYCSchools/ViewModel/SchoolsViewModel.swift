//
//  SchoolsViewModel.swift
//  NYCSchools
//
//  Created by Dipendra Khatri on 2/11/22.
//

import Foundation
import UIKit

/// Defines the interface for SchoolsViewModel
protocol SchoolsViewConfigurable {
    var isSearching: Bool { get }
    var numberOfRows: Int { get }
    var showAlert: ((String) -> Void)? { get set }
    var dataReady: (() -> Void)? { get set }
    var showSchoolDetailsScreen: ((SchoolDetailsViewConfigurable) -> Void)? { get set }
    
    func rowViewModel(for indexPath: IndexPath) -> SchoolDetailsCellViewModel?
    func handleRowSelect(for indexPath: IndexPath)
    func loadSchoolsDetails(for pageNo: Int)
    func shouldFetchNextPage(index: Int) -> Bool
    func filterSchools(for searchText: String)
}

/// Class responsible for fetching data from API and prepare for rendering list of schools
class SchoolsViewModel: SchoolsViewConfigurable {
    /// Store the object of APIClient
    let apiClient: APIClientConfigurable
    /// Number of records in each page
    let pageSize = 100
    /// Store the current page
    var currentPage = 0
    /// Store the cell view models for school list
    var schoolsCellViewModels: [SchoolDetailsCellViewModel] = []
    /// Store the cell view models for filtered school list
    var filteredSchoolsCellViewModels: [SchoolDetailsCellViewModel] = []
    /// Completion handler to show alert
    var showAlert: ((String) -> Void)?
    /// Completion handler for data ready
    var dataReady: (() -> Void)?
    /// Completion handler to show school details screen
    var showSchoolDetailsScreen: ((SchoolDetailsViewConfigurable) -> Void)?
    
    /// Initilizer
    ///
    /// - Parameter apiClient: API call object
    /// - Returns: Object
    init(apiClient: APIClientConfigurable = APIClient()) {
        self.apiClient = apiClient
    }
    
    /// Computed property to check if we are currently searching
    var isSearching: Bool {
        return !filteredSchoolsCellViewModels.isEmpty
    }
    
    /// Computed property to get the number of rows
    var numberOfRows: Int {
        return isSearching ? filteredSchoolsCellViewModels.count : schoolsCellViewModels.count
    }

    /// Check and fetch next page of data
    ///
    /// - Parameter index: index of the last displyed cell on screen
    /// - Returns: Boolean to indicate if we are fetching the next page
    func shouldFetchNextPage(index: Int) -> Bool {
        if !isSearching && (index == schoolsCellViewModels.count - 1) {
            loadSchoolsDetails(for: currentPage + 1)
            return true
        }
        return false
    }
    
    /// Retrive the cell view model for given indexPath
    ///
    /// - Parameter indexPath: indexPath of the cell
    /// - Returns: cell view model
    func rowViewModel(for indexPath: IndexPath) -> SchoolDetailsCellViewModel? {
        if isSearching && indexPath.row < filteredSchoolsCellViewModels.count {
            return filteredSchoolsCellViewModels[indexPath.row]
        } else if !isSearching && indexPath.row < schoolsCellViewModels.count {
            return schoolsCellViewModels[indexPath.row]
        } else {
            return nil
        }
    }
    
    /// Fetch the SAT score for the selected school and show school details screen
    ///
    /// - Parameter indexPath: indexPath of the cell
    /// - Returns: Void
    func handleRowSelect(for indexPath: IndexPath) {
        if let viewModel = rowViewModel(for: indexPath) {
            loadSATScore(for: viewModel.schoolDetails) { [weak self] satScores in
                let schoolDetailsVM = SchoolDetailsViewModel(viewModel.schoolDetails, satScores.first)
                self?.showSchoolDetailsScreen?(schoolDetailsVM)
            }
        }
    }
    
    /// Filter the schools based on the entered text in search bar
    ///
    /// - Parameter searchText: entered text from seach bar
    /// - Returns: Void
    func filterSchools(for searchText: String) {
        if searchText.isEmpty {
            filteredSchoolsCellViewModels.removeAll()
            return
        }
        
        filteredSchoolsCellViewModels = schoolsCellViewModels.filter { viewModel -> Bool in
            if let schoolName = viewModel.schoolDetails.schoolName,
                schoolName.contains(searchText) {
                return true
            }
            return false
        }
    }
    
    /// Setup cell view models for list of schools data
    ///
    /// - Parameter schools: List of schools data retrived from AP
    /// - Returns: Void
    private func setupCellViewModels(for schools: [SchoolDetails]) {
        schoolsCellViewModels.append(contentsOf: schools.map { SchoolDetailsCellViewModel.init(schoolDetails: $0) })
    }
}

/// API Calls
extension SchoolsViewModel {
    /// Fetch the list of schools from API in paginated way
    ///
    /// - Parameter pageNo: page number
    /// - Returns: Void
    func loadSchoolsDetails(for pageNo: Int) {
        let params = [
            "$limit": "\(pageSize)",
            "$offset": "\(pageNo)"
        ]
        
        apiClient.getData(urlString: nycSchoolsListURL, params: params) { [weak self] (result: Result<[SchoolDetails], Error>) in
            switch result {
            case .success(let schools):
                self?.currentPage = pageNo
                self?.setupCellViewModels(for: schools)
                self?.dataReady?()
            case .failure(let error):
                self?.showAlert?(error.localizedDescription)
            }
        }
    }
    
    /// Fetch the SAT score for provided school
    ///
    /// - Parameter school: school data object
    ///             completion: completion handler for API response
    /// - Returns: Void
    func loadSATScore(for school: SchoolDetails, completion: @escaping ([SATScore]) -> Void) {
        guard let dbn = school.dbn else {
            completion([])
            return
        }
        
        let params = [
            "dbn": "\(dbn)"
        ]
        
        apiClient.getData(urlString: nycSchoolSATInfoURL, params: params) { (result: Result<[SATScore], Error>) in
            switch result {
            case .success(let satScores):
                completion(satScores)
            case .failure:
                completion([])
            }
        }
    }
}
