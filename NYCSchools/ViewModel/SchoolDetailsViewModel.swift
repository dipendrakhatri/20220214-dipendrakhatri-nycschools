//
//  SchoolDetailsViewModel.swift
//  NYCSchools
//
//  Created by Dipendra Khatri on 2/12/22.
//

import Foundation
import UIKit
import MapKit

/// Defines the interface for SchoolDetailsCellViewModel
protocol SchoolDetailsViewConfigurable {
    var schoolInfo: NSAttributedString { get }
    var satMathScore: NSAttributedString { get }
    var satReadingScore: NSAttributedString { get }
    var satWritingScore: NSAttributedString { get }
    var addtionalInfoText: NSAttributedString { get }
    var location: MKCoordinateRegion? { get }
}

/// Struct responsible providing the details of school
struct SchoolDetailsViewModel: SchoolDetailsViewConfigurable {
    /// Store the school details data object
    let schoolDetails: SchoolDetails
    /// Store the SAT score data object
    let satScore: SATScore?
    /// Computed property to get the school infor like: Name, Address, Phone Number, Fax Number, Email and Website
    var schoolInfo: NSAttributedString {
        let attributedString = NSMutableAttributedString()
        if let schoolName = schoolDetails.schoolName {
            attributedString.append(NSAttributedString(string: schoolName).bolded(with: 21.0))
        }
        if let addressLine1 = schoolDetails.addressLine1,
           let city = schoolDetails.city,
           let state = schoolDetails.stateCode,
           let zip = schoolDetails.zip {
            let address = "\(addressLine1), \(city) \(state) \(zip)"
            attributedString.append(NSAttributedString(string: "\n"))
            attributedString.append(NSAttributedString(string: address).font(with: 17.0))
        }
        if let phoneNumber = schoolDetails.phoneNumber {
            let phoneNumberText = "Phone: \(phoneNumber)"
            attributedString.append(NSAttributedString(string: "\n"))
            attributedString.append(NSAttributedString(string: phoneNumberText).font(with: 14.0))
        }
        if let faxNumber = schoolDetails.faxNumber {
            let faxNumberText = "Fax: \(faxNumber)"
            attributedString.append(NSAttributedString(string: "\n"))
            attributedString.append(NSAttributedString(string: faxNumberText).font(with: 14.0))
        }
        if let email = schoolDetails.schoolEmail {
            let emailText = "Email: \(email)"
            attributedString.append(NSAttributedString(string: "\n"))
            attributedString.append(NSAttributedString(string: emailText).font(with: 14.0))
        }
        if let website = schoolDetails.website {
            let websiteText = "Website: \(website)"
            attributedString.append(NSAttributedString(string: "\n"))
            attributedString.append(NSAttributedString(string: websiteText).font(with: 14.0))
        }
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 4
        return attributedString.applying(attributes: [.paragraphStyle : style])
    }
    /// Computed property to get SAT math score
    var satMathScore: NSAttributedString {
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "Math").bolded(with: 17.0))
        attributedString.append(NSAttributedString(string: "\n"))
        attributedString.append(NSAttributedString(string: satScore?.mathAvgScore ?? "N/A").font(with: 17.0))
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        style.lineSpacing = 4
        return attributedString.applying(attributes: [.paragraphStyle : style])
    }
    /// Computed property to get SAT reading score
    var satReadingScore: NSAttributedString {
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "Reading").bolded(with: 17.0))
        attributedString.append(NSAttributedString(string: "\n"))
        attributedString.append(NSAttributedString(string: satScore?.readingAvgScore ?? "N/A").font(with: 17.0))
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        style.lineSpacing = 4
        return attributedString.applying(attributes: [.paragraphStyle : style])
    }
    /// Computed property to get SAT writing score
    var satWritingScore: NSAttributedString {
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "Writing").bolded(with: 17.0))
        attributedString.append(NSAttributedString(string: "\n"))
        attributedString.append(NSAttributedString(string: satScore?.writingAvgScore ?? "N/A").font(with: 17.0))
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        style.lineSpacing = 4
        return attributedString.applying(attributes: [.paragraphStyle : style])
    }
    /// Computed property to get school addtional Info
    var addtionalInfoText: NSAttributedString {
        let attributedString = NSMutableAttributedString()
        if let overview = schoolDetails.overview {
            attributedString.append(NSAttributedString(string: "Overview\n").bolded(with: 21.0))
            attributedString.append(NSAttributedString(string: overview).font(with: 14.0))
        }
        if let program = schoolDetails.program {
            attributedString.append(NSAttributedString(string: "\n\nProgram\n").bolded(with: 21.0))
            attributedString.append(NSAttributedString(string: program).font(with: 14.0))
        }
        if let interest = schoolDetails.interest {
            attributedString.append(NSAttributedString(string: "\n\nInterest\n").bolded(with: 21.0))
            attributedString.append(NSAttributedString(string: interest).font(with: 14.0))
        }
        var requirements = ""
        if let requirement1 = schoolDetails.requirement1 {
            requirements += requirement1
        }
        if let requirement2 = schoolDetails.requirement2 {
            requirements += "\n"
            requirements += requirement2
        }
        if let requirement3 = schoolDetails.requirement3 {
            requirements += "\n"
            requirements += requirement3
        }
        if let requirement4 = schoolDetails.requirement4 {
            requirements += "\n"
            requirements += requirement4
        }
        if let requirement5 = schoolDetails.requirement5 {
            requirements += "\n"
            requirements += requirement5
        }
        if !requirements.isEmpty {
            attributedString.append(NSAttributedString(string: "\n\nRequirements\n").bolded(with: 21.0))
            attributedString.append(NSAttributedString(string: requirements).font(with: 14.0))
        }
        var priority = ""
        if let admissionsPriority1 = schoolDetails.admissionsPriority1 {
            priority += admissionsPriority1
        }
        if let admissionsPriority2 = schoolDetails.admissionsPriority2 {
            priority += "\n"
            priority += admissionsPriority2
        }
        if let admissionsPriority3 = schoolDetails.admissionsPriority3 {
            priority += "\n"
            priority += admissionsPriority3
        }
        if !priority.isEmpty {
            attributedString.append(NSAttributedString(string: "\n\nAdmissions Priority\n").bolded(with: 21.0))
            attributedString.append(NSAttributedString(string: priority).font(with: 14.0))
        }
        var opportunities = ""
        if let academicOpportunities1 = schoolDetails.academicOpportunities1 {
            opportunities += academicOpportunities1
        }
        if let academicOpportunities2 = schoolDetails.academicOpportunities2 {
            opportunities += "\n"
            opportunities += academicOpportunities2
        }
        if !opportunities.isEmpty {
            attributedString.append(NSAttributedString(string: "\n\nAcademic Opportunities\n").bolded(with: 21.0))
            attributedString.append(NSAttributedString(string: opportunities).font(with: 14.0))
        }
        if let schoolSports = schoolDetails.schoolSports {
            attributedString.append(NSAttributedString(string: "\n\nSports\n").bolded(with: 21.0))
            attributedString.append(NSAttributedString(string: schoolSports).font(with: 14.0))
        }
        if let activities = schoolDetails.extracurricularActivities {
            attributedString.append(NSAttributedString(string: "\n\nExtracurricular Activities\n").bolded(with: 21.0))
            attributedString.append(NSAttributedString(string: activities).font(with: 14.0))
        }
        if let subway = schoolDetails.subway {
            attributedString.append(NSAttributedString(string: "\n\nSubway\n").bolded(with: 21.0))
            attributedString.append(NSAttributedString(string: subway).font(with: 14.0))
        }
        if let bus = schoolDetails.bus {
            attributedString.append(NSAttributedString(string: "\n\nBus\n").bolded(with: 21.0))
            attributedString.append(NSAttributedString(string: bus).font(with: 14.0))
        }
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 4
        return attributedString.applying(attributes: [.paragraphStyle : style])
    }
    /// Computed property to get school location for Map
    var location: MKCoordinateRegion? {
        if let latitude = Double(schoolDetails.latitude ?? ""),
           let longitude = Double(schoolDetails.longitude ?? "") {
            let location = CLLocation(latitude: latitude, longitude: longitude)
            return MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 100, longitudinalMeters: 100)
        }
        return nil
    }
    
    /// Initilizer
    ///
    /// - Parameter schoolDetails: school details data object
    ///             satScore: SAT Score data object
    /// - Returns: Object
    init(_ schoolDetails: SchoolDetails, _ satScore: SATScore?) {
        self.schoolDetails = schoolDetails
        self.satScore = satScore
    }
}
