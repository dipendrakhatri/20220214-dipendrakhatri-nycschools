//
//  SchoolDetailsCellViewModel.swift
//  NYCSchools
//
//  Created by Dipendra Khatri on 2/11/22.
//

import Foundation
import UIKit

/// Defines the interface for SchoolDetailsCellViewModel
protocol SchoolDetailsCellViewConfigurable {
    var schoolName: String? { get }
    var address: String? { get }
    var phoneNumber: String? { get }
    var interest: String? { get }
    var program: String? { get }
    var schoolDetails: SchoolDetails { get }
}

/// Struct responsible for providing the read-only text to UITableViewCell
struct SchoolDetailsCellViewModel: SchoolDetailsCellViewConfigurable {
    /// Store the school details data object
    let schoolDetails: SchoolDetails
    /// Computed property to get school name
    var schoolName: String? { schoolDetails.schoolName }
    /// Computed property to get school address
    var address: String? {
        guard let addressLine1 = schoolDetails.addressLine1,
              let city = schoolDetails.city,
              let state = schoolDetails.stateCode,
              let zip = schoolDetails.zip else { return nil }
        
        var completeAddress = addressLine1
        completeAddress += ", "
        completeAddress += "\(city) \(state) \(zip)"
        return completeAddress
    }
    /// Computed property to get school phone number
    var phoneNumber: String? { schoolDetails.phoneNumber }
    /// Computed property to get school interest
    var interest: String? {
        guard let interest = schoolDetails.interest else { return nil }
        return "Interest: " + interest
    }
    /// Computed property to get school program
    var program: String? {
        guard let program = schoolDetails.program else { return nil }
        return "Program: " + program
    }
    
    /// Initilizer
    ///
    /// - Parameter schoolDetails: school details data object
    /// - Returns: Object
    init(schoolDetails: SchoolDetails) {
        self.schoolDetails = schoolDetails
    }
}
